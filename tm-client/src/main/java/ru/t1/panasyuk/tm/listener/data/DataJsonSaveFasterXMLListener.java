package ru.t1.panasyuk.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.panasyuk.tm.dto.request.data.DataJsonSaveFasterXMLRequest;
import ru.t1.panasyuk.tm.event.ConsoleEvent;

@Component
public final class DataJsonSaveFasterXMLListener extends AbstractDataListener {

    @NotNull
    private static final String DESCRIPTION = "Save data in json file.";

    @NotNull
    private static final String NAME = "data-save-json-fasterxml";

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataJsonSaveFasterXMLListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[DATA SAVE JSON]");
        @NotNull final DataJsonSaveFasterXMLRequest request = new DataJsonSaveFasterXMLRequest(getToken());
        domainEndpoint.saveDataJsonFasterXML(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
