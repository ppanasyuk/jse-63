package ru.t1.panasyuk.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.panasyuk.tm.dto.request.data.DataYamlSaveFasterXMLRequest;
import ru.t1.panasyuk.tm.event.ConsoleEvent;

@Component
public final class DataYamlSaveFasterXMLListener extends AbstractDataListener {

    @NotNull
    private static final String DESCRIPTION = "Save data in yaml file.";

    @NotNull
    private static final String NAME = "data-save-yaml-fasterxml";

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataYamlSaveFasterXMLListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[DATA SAVE YAML]");
        @NotNull final DataYamlSaveFasterXMLRequest request = new DataYamlSaveFasterXMLRequest(getToken());
        domainEndpoint.saveDataYamlFasterXML(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
