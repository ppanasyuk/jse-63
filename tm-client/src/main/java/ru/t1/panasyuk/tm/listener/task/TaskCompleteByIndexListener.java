package ru.t1.panasyuk.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.panasyuk.tm.dto.request.task.TaskCompleteByIndexRequest;
import ru.t1.panasyuk.tm.dto.response.task.TaskCompleteByIndexResponse;
import ru.t1.panasyuk.tm.event.ConsoleEvent;
import ru.t1.panasyuk.tm.util.TerminalUtil;

@Component
public final class TaskCompleteByIndexListener extends AbstractTaskListener {

    @NotNull
    private static final String DESCRIPTION = "Complete task by index.";

    @NotNull
    private static final String NAME = "task-complete-by-index";

    @Override
    @EventListener(condition = "@taskCompleteByIndexListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[COMPLETE TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber();
        @NotNull final TaskCompleteByIndexRequest request = new TaskCompleteByIndexRequest(getToken(), index);
        @NotNull final TaskCompleteByIndexResponse response = taskEndpoint.completeTaskByIndex(request);
        if (!response.getSuccess()) System.out.println(response.getMessage());
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}