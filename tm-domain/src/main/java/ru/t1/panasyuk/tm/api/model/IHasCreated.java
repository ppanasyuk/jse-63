package ru.t1.panasyuk.tm.api.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Date;

public interface IHasCreated {

    @NotNull
    Date getCreated();

    void setCreated(@NotNull Date created);

}