package ru.t1.panasyuk.tm.listener;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.panasyuk.tm.api.ILoggerService;
import ru.t1.panasyuk.tm.api.IPropertyService;
import ru.t1.panasyuk.tm.service.LoggerService;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

@Component
public final class EntityListener implements MessageListener {

    @NotNull
    @Autowired
    private ILoggerService loggerService;

    @Override
    @SneakyThrows
    public void onMessage(@NotNull final Message message) {
        final boolean checkType = message instanceof TextMessage;
        if (!checkType) return;
        final TextMessage textMessage = (TextMessage) message;
        final String text = textMessage.getText();
        loggerService.log(text);
    }

}