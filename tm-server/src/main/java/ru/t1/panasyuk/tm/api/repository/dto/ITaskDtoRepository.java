package ru.t1.panasyuk.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.dto.model.TaskDTO;

import java.util.List;

public interface ITaskDtoRepository extends IUserOwnedDtoRepository<TaskDTO> {

    @Nullable
    List<TaskDTO> findAllByProjectId(@NotNull String userId, @Nullable String projectId);

    void removeAllByProjectId(@NotNull String userId, @Nullable String projectId);

}