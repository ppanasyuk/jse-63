package ru.t1.panasyuk.tm.log;

public enum OperationType {

    INSERT,
    DELETE,
    UPDATE

}