package ru.t1.panasyuk.tm.service.model;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.panasyuk.tm.api.service.model.IProjectService;
import ru.t1.panasyuk.tm.comparator.CreatedComparator;
import ru.t1.panasyuk.tm.comparator.NameComparator;
import ru.t1.panasyuk.tm.comparator.StatusComparator;
import ru.t1.panasyuk.tm.constant.FieldConst;
import ru.t1.panasyuk.tm.enumerated.Sort;
import ru.t1.panasyuk.tm.enumerated.Status;
import ru.t1.panasyuk.tm.exception.entity.EntityNotFoundException;
import ru.t1.panasyuk.tm.exception.entity.ProjectNotFoundException;
import ru.t1.panasyuk.tm.exception.field.*;
import ru.t1.panasyuk.tm.model.Project;
import ru.t1.panasyuk.tm.model.User;
import ru.t1.panasyuk.tm.repository.model.ProjectRepository;
import ru.t1.panasyuk.tm.repository.model.UserRepository;

import java.util.Comparator;
import java.util.List;

@Service
public final class ProjectService extends AbstractUserOwnedService<Project, ProjectRepository>
        implements IProjectService {

    @Getter
    @NotNull
    @Autowired
    private UserRepository userRepository;

    @NotNull
    @Override
    @Transactional
    public Project changeProjectStatusById(
            @NotNull final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (id == null || id.isEmpty()) throw new ProjectIdEmptyException();
        if (status == null) throw new StatusIncorrectException();
        @Nullable final Project project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        update(project);
        return project;
    }

    @NotNull
    @Override
    @Transactional
    public Project changeProjectStatusByIndex(
            @NotNull final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index > getSize(userId)) throw new IndexIncorrectException();
        if (status == null) throw new StatusIncorrectException();
        @Nullable final Project project = findOneByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        update(project);
        return project;
    }

    @Override
    @Transactional
    public void clear(@NotNull final String userId) {
        @NotNull final ProjectRepository repository = getRepository();
        repository.deleteByUserId(userId);
    }

    @NotNull
    @Override
    @Transactional
    public Project create(
            @NotNull final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final UserRepository userRepository = getUserRepository();
        @NotNull final Project project = new Project();
        @Nullable final User user = userRepository.findById(userId).get();
        project.setName(name);
        project.setDescription(description);
        project.setUser(user);
        add(project);
        return project;
    }

    @NotNull
    @Override
    @Transactional
    public Project create(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final UserRepository userRepository = getUserRepository();
        @NotNull final Project project = new Project();
        @Nullable final User user = userRepository.findById(userId).get();
        project.setName(name);
        project.setUser(user);
        add(project);
        return project;
    }

    @Override
    public boolean existsById(@NotNull final String userId, @Nullable final String id) {
        boolean result;
        @NotNull final ProjectRepository repository = getRepository();
        result = repository.findById(userId, id) != null;
        return result;
    }

    @Nullable
    @Override
    public List<Project> findAll(@NotNull final String userId, @Nullable final Comparator comparator) {
        @Nullable List<Project> models;
        @NotNull final ProjectRepository repository = getRepository();
        @NotNull String field = FieldConst.FIELD_CREATED;
        if (comparator == NameComparator.INSTANCE) field = FieldConst.FIELD_NAME;
        else if (comparator == StatusComparator.INSTANCE) field = FieldConst.FIELD_STATUS;
        @NotNull org.springframework.data.domain.Sort sort = org.springframework.data.domain.Sort.by(
                org.springframework.data.domain.Sort.Direction.DESC,
                field);
        models = repository.findByUserId(userId, sort);
        return models;
    }

    @Nullable
    @Override
    @SuppressWarnings("unchecked")
    public List<Project> findAll(@NotNull final String userId, @Nullable final Sort sort) {
        if (sort == null) return findAll(userId);
        final Comparator<Project> comparator = sort.getComparator();
        return findAll(userId, comparator);
    }

    @Nullable
    @Override
    public List<Project> findAll(@NotNull final String userId) {
        @Nullable final List<Project> models;
        @NotNull final ProjectRepository repository = getRepository();
        models = findAll(userId, CreatedComparator.INSTANCE);
        return models;
    }

    @Nullable
    @Override
    public Project findOneById(@NotNull final String userId, @Nullable final String id) {
        if (id == null) return null;
        @Nullable final Project model;
        @NotNull final ProjectRepository repository = getRepository();
        model = repository.findById(userId, id);
        return model;
    }

    @Nullable
    @Override
    public Project findOneByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index > getSize(userId)) throw new IndexIncorrectException();
        @Nullable final Project model;
        @NotNull final ProjectRepository repository = getRepository();
        @NotNull final Pageable pageable = PageRequest.of(index - 1, 1);
        model = repository
                .findByIndex(userId, pageable)
                .stream()
                .findFirst()
                .orElse(null);
        return model;
    }

    @Override
    public long getSize(@NotNull final String userId) {
        long result;
        @NotNull final ProjectRepository repository = getRepository();
        result = repository.countByUserId(userId);
        return result;
    }

    @Nullable
    @Override
    @Transactional
    public Project removeById(@NotNull final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Project removedModel;
        @NotNull final ProjectRepository repository = getRepository();
        removedModel = repository.findById(userId, id);
        if (removedModel == null) throw new EntityNotFoundException();
        repository.delete(removedModel);
        return removedModel;
    }

    @Nullable
    @Override
    @Transactional
    public Project removeByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index > getSize(userId)) throw new IndexIncorrectException();
        @Nullable final Project removedModel;
        @NotNull final ProjectRepository repository = getRepository();
        @NotNull final Pageable pageable = PageRequest.of(index - 1, 1);
        removedModel = repository
                .findByIndex(userId, pageable)
                .stream()
                .findFirst()
                .orElseThrow(EntityNotFoundException::new);
        repository.delete(removedModel);
        return removedModel;
    }

    @NotNull
    @Override
    @Transactional
    public Project updateById(
            @NotNull final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable final Project project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        update(project);
        return project;
    }

    @NotNull
    @Override
    @Transactional
    public Project updateByIndex(
            @NotNull final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index > getSize(userId)) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable final Project project = findOneByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        update(project);
        return project;
    }

}