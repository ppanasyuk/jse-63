package ru.t1.panasyuk.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.panasyuk.tm.api.service.model.ISessionService;
import ru.t1.panasyuk.tm.exception.entity.EntityNotFoundException;
import ru.t1.panasyuk.tm.exception.field.IdEmptyException;
import ru.t1.panasyuk.tm.exception.field.IndexIncorrectException;
import ru.t1.panasyuk.tm.model.Session;
import ru.t1.panasyuk.tm.repository.model.SessionRepository;

import java.util.List;

@Service
public final class SessionService extends AbstractUserOwnedService<Session, SessionRepository>
        implements ISessionService {

    @Override
    @Transactional
    public void clear(@NotNull final String userId) {
        @NotNull final SessionRepository repository = getRepository();
        repository.deleteByUserId(userId);
    }

    @Override
    public boolean existsById(@NotNull final String userId, @Nullable final String id) {
        boolean result;
        @NotNull final SessionRepository repository = getRepository();
        result = repository.findById(userId, id) != null;
        return result;
    }

    @Nullable
    @Override
    public List<Session> findAll() {
        @Nullable final List<Session> sessions;
        @NotNull final SessionRepository repository = getRepository();
        sessions = repository.findAll();
        return sessions;
    }

    @Nullable
    @Override
    public List<Session> findAll(@NotNull final String userId) {
        @Nullable final List<Session> models;
        @NotNull final SessionRepository repository = getRepository();
        models = repository.findAllOrderByCreated(userId);
        return models;
    }

    @Nullable
    @Override
    public Session findOneById(@NotNull final String userId, @Nullable final String id) {
        if (id == null) return null;
        @Nullable final Session model;
        @NotNull final SessionRepository repository = getRepository();
        model = repository.findById(userId, id);
        return model;
    }

    @Nullable
    @Override
    public Session findOneByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index > getSize(userId)) throw new IndexIncorrectException();
        @Nullable final Session model;
        @NotNull final SessionRepository repository = getRepository();
        @NotNull final Pageable pageable = PageRequest.of(index - 1, 1);
        model = repository
                .findByIndex(userId, pageable)
                .stream()
                .findFirst()
                .orElse(null);
        return model;
    }

    @Override
    public long getSize(@NotNull final String userId) {
        long result;
        @NotNull final SessionRepository repository = getRepository();
        result = repository.countByUserId(userId);
        return result;
    }

    @NotNull
    @Override
    @Transactional
    public Session remove(@Nullable final Session session) {
        if (session == null) throw new EntityNotFoundException();
        @NotNull final SessionRepository repository = getRepository();
        repository.delete(session);
        return session;
    }

    @Nullable
    @Override
    @Transactional
    public Session removeById(@NotNull final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Session removedModel;
        @NotNull final SessionRepository repository = getRepository();
        removedModel = repository.findById(userId, id);
        if (removedModel == null) throw new EntityNotFoundException();
        repository.delete(removedModel);
        return removedModel;
    }

    @Nullable
    @Override
    @Transactional
    public Session removeByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index > getSize(userId)) throw new IndexIncorrectException();
        @Nullable final Session removedModel;
        @NotNull final SessionRepository repository = getRepository();
        @NotNull final Pageable pageable = PageRequest.of(index - 1, 1);
        removedModel = repository
                .findByIndex(userId, pageable)
                .stream()
                .findFirst()
                .orElseThrow(EntityNotFoundException::new);
        repository.delete(removedModel);
        return removedModel;
    }

}