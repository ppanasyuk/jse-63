package ru.t1.panasyuk.tm.migration;

import io.qameta.allure.junit4.DisplayName;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.panasyuk.tm.AbstractSchemeTest;
import ru.t1.panasyuk.tm.api.repository.dto.ISessionDtoRepository;
import ru.t1.panasyuk.tm.api.service.IPropertyService;
import ru.t1.panasyuk.tm.configuration.ServerConfiguration;
import ru.t1.panasyuk.tm.dto.model.SessionDTO;
import ru.t1.panasyuk.tm.service.PropertyService;

import javax.persistence.EntityManager;

@Ignore
@DisplayName("Тестирование создания схемы для сессий")
public class SessionSchemeTest extends AbstractSchemeTest {

    @Test
    @DisplayName("Создание схемы")
    public void test() throws LiquibaseException {
        liquibase.update("session");
        context = new AnnotationConfigApplicationContext(ServerConfiguration.class);
        @NotNull final SessionDTO session = createSession();
        Assert.assertNotNull(session);
        int countOfSessions = getCountOfSessions();
        Assert.assertEquals(1, countOfSessions);
        deleteSession(session);
    }

    @NotNull
    private SessionDTO createSession() {
        @NotNull final ISessionDtoRepository sessionRepository = context.getBean(ISessionDtoRepository.class);
        @NotNull final EntityManager entityManager = sessionRepository.getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final SessionDTO sessionDTO = new SessionDTO();
        sessionRepository.add(sessionDTO);
        entityManager.getTransaction().commit();
        entityManager.close();
        return sessionDTO;
    }

    private int getCountOfSessions() {
        @NotNull final ISessionDtoRepository sessionRepository = context.getBean(ISessionDtoRepository.class);
        @NotNull final EntityManager entityManager = sessionRepository.getEntityManager();
        entityManager.getTransaction().begin();
        final int count = sessionRepository.getSize();
        entityManager.getTransaction().commit();
        entityManager.close();
        return count;
    }

    private void deleteSession(@NotNull final SessionDTO session) {
        @NotNull final ISessionDtoRepository sessionRepository = context.getBean(ISessionDtoRepository.class);
        @NotNull final EntityManager entityManager = sessionRepository.getEntityManager();
        entityManager.getTransaction().begin();
        sessionRepository.remove(session);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

}