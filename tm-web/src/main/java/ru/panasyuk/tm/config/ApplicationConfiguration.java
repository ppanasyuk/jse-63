package ru.panasyuk.tm.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("ru.panasyuk.tm")
public class ApplicationConfiguration {
}