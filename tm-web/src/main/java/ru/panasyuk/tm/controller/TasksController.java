package ru.panasyuk.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.panasyuk.tm.model.Project;
import ru.panasyuk.tm.repository.ProjectRepository;
import ru.panasyuk.tm.repository.TaskRepository;

import java.util.Collection;

@Controller
public class TasksController {

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private ProjectRepository projectRepository;

    @GetMapping("/tasks")
    public ModelAndView index() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task-list");
        modelAndView.addObject("tasks", taskRepository.findAll());
        modelAndView.addObject("projectRepository", projectRepository);
        return modelAndView;
    }

}